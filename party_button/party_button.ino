#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "private_header.h"
 
WiFiClient espClient;
PubSubClient client(espClient);
 
void setup() {
  Serial.begin(9600);
  
  pinMode(5, OUTPUT); // Used to shut the power off

  delay(10);
 
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("ESP8266 Connecting...");
   
  WiFi.begin(wifi_ssid, wifi_password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
 
  Serial.println("");
  Serial.println("WiFi connected"); 
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  
  Serial.println("MQTT");

  if (client.connect(mqtt_server, mqtt_user, mqtt_pass)) {
      client.publish(mqtt_out_topic, "on");
  }
 
  delay(2000); // Make sure the mqtt msg gets published.
  Serial.println("Shutting down.");
  digitalWrite(5, HIGH);
}
 
void loop() {}
